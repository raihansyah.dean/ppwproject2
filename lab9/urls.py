from django.contrib import admin
from django.urls import path
from . import views

app_name = 'lab9'
urlpatterns = [
    path('', views.homepage, name='index_lab9'),
    path('data/', views.data, name='books_data'),
]
