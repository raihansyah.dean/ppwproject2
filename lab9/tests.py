from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import homepage
import unittest
import time

# Create your tests here.
class Lab9_Test(TestCase):
    def test_lab_9_url_exists(self):
        response = Client().get('/lab9/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_exists(self):
        response = Client().get('/lab9/data/')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_booklist_template(self):
        response = Client().get('/lab9/')
        self.assertTemplateUsed(response, 'booklist.html')

    def test_lab9_using_homepage_func(self):
        found = resolve('/lab9/')
        self.assertEqual(found.func, homepage)
    
    def test_lab9_index_header(self):
        request = HttpRequest()
        response = homepage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Books List', html_response)

class Lab9_FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options) 
        super(Lab9_FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab9_FunctionalTest, self).tearDown()

    def test_layout_page_title(self):
        browser = self.browser
        browser.get(self.live_server_url + '/lab9/')
        self.assertIn('My Books List', self.browser.title)
        time.sleep(2)

    def test_layout_header_with_css_property(self):
        browser = self.browser
        browser.get(self.live_server_url + '/lab9/')
        content = browser.find_element_by_tag_name('p').value_of_css_property('color')
        self.assertIn('rgba(255, 255, 255, 1)', content)
        time.sleep(2)

