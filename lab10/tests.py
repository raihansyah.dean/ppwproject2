from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from django.db import IntegrityError
from .views import homepage, check_email
from .models import Subscriber
from .forms import FormSubscriber
import unittest

# Create your tests here.
class Lab10_Test(TestCase):
    def test_lab10_url_is_exist(self):
        response = Client().get('/lab10/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_using_subscribe_template(self):
        response = Client().get('/lab10/')
        self.assertTemplateUsed(response, 'subscribers.html')

    def test_lab10_using_subscribe_func(self):
        found = resolve('/lab10/')
        self.assertEqual(found.func, homepage)

    def test_lab10_using_checkEmail_func(self):
        found = resolve('/lab10/check_email/')
        self.assertEqual(found.func, check_email)

    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="me", email="itsme@yahoo.com", password="hello1234")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)

    def test_FormSubscriber_valid(self):
        form = FormSubscriber(data={'name': "me", 'email': "user@yahoo.com" , 'password': "user1234"})
        self.assertTrue(form.is_valid())
    
    def test_unique_email(self):
        Subscriber.objects.create(email="dean@email.com")
        with self.assertRaises(IntegrityError):
            Subscriber.objects.create(email="dean@email.com")
        
    def test_check_email_view_get_return_200(self):
        email = "dean@gmail.com"
        Client().post('/lab10/check_email/', {'email': email})
        response = Client().post('/lab10/', {'email': 'dean@gmail.com'})
        self.assertEqual(response.status_code, 200)

    def test_check_email_already_exist_view_get_return_200(self):
        Subscriber.objects.create(name="me", email="yooo@gmail.com", password="abcde")
        response = Client().post('/lab10/check_email/', {
            "email": "yooo@gmail.com"
        })
        self.assertEqual(response.json()['is_email'], True)

    def test_homepage_should_return_status_subscribe_true(self):
        response = Client().post('/lab10/', {
            "name": "capekbeneran",
            "email": "dean@gmail.com",
            "password":  "password",
        })
        self.assertEqual(response.json()['status_subscribe'], True)

    def test_homepage_should_return_status_subscribe_false(self):
        name, email, password = "dean", "dean@gmail.com", "password"
        Subscriber.objects.create(name=name, email=email, password=password)
        response = Client().post('/lab10/', {
            "name": name,
            "email": email,
            "password":  password,
        })
        self.assertEqual(response.json()['status_subscribe'], False)
