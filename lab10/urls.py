from django.contrib import admin
from django.urls import path
from . import views

app_name = 'lab10'
urlpatterns = [
    path('', views.homepage, name = 'homepage'),
    path('check_email/', views.check_email, name = 'check_email'),
    path('list-subscriber/', views.listSubscriber, name='listSubscriber'),
    path('unsubscribe_pass/<password>/<email>/', views.unsubscribe_pass, name='unsubscribe'),
]