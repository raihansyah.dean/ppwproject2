from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from .models import Subscriber
from .forms import FormSubscriber
from django.views.decorators.csrf import csrf_exempt

response = {}
def homepage(request):
    response['activetab'] = 'subscriberpage'
    if request.method == 'POST':
        form = FormSubscriber(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status_subscribe = True

            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status_subscribe = False

            return JsonResponse({'status_subscribe' : status_subscribe})

    Form = FormSubscriber()
    response['form'] = Form
    return render(request, 'subscribers.html', response)

def check_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        validasi = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email' : validasi})

def listSubscriber(request):
    all_subs = Subscriber.objects.all().values()
    subs = list(all_subs)
    return JsonResponse({'all_subs' : subs})

@csrf_exempt
def unsubscribe_pass(request, password, email):
    if request.method == 'POST':
        email = request.POST['email']
        sub = Subscriber.objects.get(email=email)

        if sub.password == password:
        	sub.delete()
        	return JsonResponse({})

       	else:
       		return JsonResponse({})

        
