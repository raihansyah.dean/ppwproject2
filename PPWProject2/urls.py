"""PPWProject2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include,url
from django.contrib import admin
import lab6.urls as lab6
import lab8.urls as lab8
import lab9.urls as lab9
import lab10.urls as lab10

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^lab6/', include(lab6,namespace='lab6')),
    url(r'^lab8/', include(lab8,namespace='lab8')),
    url(r'^lab9/', include(lab9,namespace='lab9')),
    url(r'^lab10/', include(lab10,namespace='lab10')),
    url(r'^lab11/', include("lab11.urls")),
]
