$(document).ready(function(){

	$(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
        });
    });

	$("input").change(function() {
      if(this.checked){
          $('link[href="/static/css/lab8-light-theme.css"]').attr('href', '/static/css/lab8-dark-theme.css');
          document.getElementById('night').innerHTML = '<strong>Daylight?</strong>';
        } else {
          $('link[href="/static/css/lab8-dark-theme.css"]').attr('href', '/static/css/lab8-light-theme.css');
          document.getElementById('night').innerHTML = '<strong>Night Mode?</strong>';
        }
    });

});

setTimeout(function() {
    setTimeout(function() {
        $('.pageLoad').addClass('off');
    }, 0)
    setTimeout(function(){
        $("#page").css({"visibility":"visible"});
    },300)
}, 1500)