from django.conf.urls import url

from .views import tampil_profil

app_name = 'lab6'
urlpatterns = [
    url(r'^$', tampil_profil, name='tampil_profil'),
]