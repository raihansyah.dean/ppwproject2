from django.test import TestCase, Client
from django.urls import resolve
from .views import tampil_profil
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
import time
from selenium.webdriver.chrome.options import Options

# # Create your tests here.
class Lab8Test(TestCase):
	def test_lab8_url_is_exist(self):
		""" Check if landing page return 200 """
		response = Client().get('/lab8/')
		self.assertEqual(response.status_code,200)

	def test_lab8_url_is_not_exist(self):
		""" Check if not exist page return 404 """
		response = Client().get('/haha/')
		self.assertEqual(response.status_code,404)

	def test_lab8_profile_contain_title(self):
		name = "Hi! I'm Dean"
		response = Client().get('/lab8/')
		html_response = response.content.decode('utf8')
		self.assertIn(name, html_response)
\

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_js_theme_changed(self):
		browser = self.browser
		browser.get(self.live_server_url + '/lab8/')
		time.sleep(2)

		body_background = browser.find_element_by_id('body').value_of_css_property('background')
		self.assertIn("rgb(255, 255, 255) linear-gradient(to right, rgb(212, 41, 0), rgb(255, 133, 63))", body_background)
		time.sleep(2)

		browser.find_element_by_class_name('switch').click()
		body_background = browser.find_element_by_id('body').value_of_css_property('background')
		self.assertIn("rgb(255, 255, 255) linear-gradient(to right, rgb(212, 41, 0), rgb(255, 133, 63))", body_background)
	
	def test_js_accordian_one(self):
		browser = self.browser
		browser.get(self.live_server_url + '/lab8/')
		time.sleep(2)
		browser.find_element_by_xpath('//*[@id="ui-id-1"]').click()
		self.assertIn("The usual college life", browser.page_source) 
		time.sleep(2)

	def test_js_accordian_two(self):
		browser = self.browser
		browser.get(self.live_server_url + '/lab8/')
		time.sleep(2)
		browser.find_element_by_xpath('//*[@id="ui-id-3"]').click()
		self.assertIn("Finance Manager Compfest 2019", browser.page_source) 
		time.sleep(2)

	def test_js_accordian_three(self):
		browser = self.browser
		browser.get(self.live_server_url + '/lab8/')
		time.sleep(2)
		browser.find_element_by_xpath('//*[@id="ui-id-5"]').click()
		self.assertIn("ICPC Regional Jakarta 2018, Finalist", browser.page_source) 
		time.sleep(2)
