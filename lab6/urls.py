from django.conf.urls import url

from .views import homepage, tampil_profil

app_name = 'lab6'
urlpatterns = [
    url(r'^$', homepage, name='homepage'),
    url(r'^profil', tampil_profil, name='tampil_profil')
]