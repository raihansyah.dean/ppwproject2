from django.db import models
from django.utils import timezone

class Status(models.Model):
	message = models.CharField(max_length = 300, blank = False)
	created_at = models.DateTimeField(default=timezone.now)