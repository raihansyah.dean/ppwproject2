from django.shortcuts import render, redirect
from .models import Status
from .forms import Make_Status

def homepage(request):
	form = Make_Status(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if (form.is_valid()):
			Status.objects.create(message=request.POST.get("message"))
			return redirect('lab6:homepage')

	status = Status.objects.all().order_by("-id")
	response = {
		"status" : status,
		"form" : form
	}
	return render(request, 'landingpage.html', response)

def tampil_profil(request):
	return render(request, 'profile.html')
