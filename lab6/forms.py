from django import forms
from .models import Status

class Make_Status(forms.Form):
	message = forms.CharField(label = "Status", max_length= 300,widget=forms.TextInput(attrs = {'placeholder' : 'Something new?'}))