from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage, tampil_profil
from .models import Status
from django.test import LiveServerTestCase
from selenium import webdriver
import unittest
import time
from selenium.webdriver.chrome.options import Options

class Lab6Test(TestCase):
    def test_lab6_url_is_exist(self):
    	""" Check if landing page return 200 """
    	response = Client().get('/lab6/')
    	self.assertEqual(response.status_code,200)

    def test_lab6_profil_url_is_exist(self):
    	""" Check if profile page return 200 """
    	response = Client().get('/lab6/profil')
    	self.assertEqual(response.status_code,200)

    def test_lab6_using_homepage_func(self):
    	""" Check if views is using 'homepage' function """
    	found = resolve('/lab6/')
    	self.assertEqual(found.func, homepage)

    def test_lab6_using_tampil_profil_func(self):
    	""" Check if views is using 'tampil_profil' function """
    	found = resolve('/lab6/profil')
    	self.assertEqual(found.func, tampil_profil)

    def test_status_request_post_success(self):
    	""" Check if reponse can be post """
    	message = 'Hai ini test'
    	response_post = Client().post('/lab6/', {'message': message})
    	self.assertEqual(response_post.status_code, 302)

    def test_model_can_create_new_activity(self):
    	""" Check if status was created """
    	new_activity = Status.objects.create(message = "Test Message Status")
    	counting_all_available_activity = Status.objects.all().count()
    	self.assertEqual(counting_all_available_activity,1)


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')

        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_enter_status_text_and_posted_on_page(self):
        self.browser.get(self.live_server_url + "/lab6/")
        time.sleep(2)
        self.assertIn('Current Status?', self.browser.title)
        time.sleep(2)

        text = self.browser.find_element_by_id('id_message')
        submit = self.browser.find_element_by_tag_name('input')

        text.send_keys('Coba Coba')
        time.sleep(2)
        submit.submit()
        time.sleep(2)

        message_text = self.browser.find_element_by_class_name('messages').text
        self.assertIn('Coba Coba', message_text)
        time.sleep(2)

    def test_for_html_layout_positioning(self):
        self.browser.get('http://terlalu-sibuk.herokuapp.com/lab6/')
        time.sleep(2)

        title_text = self.browser.find_element_by_class_name('title2').text
        self.assertIn('Hello! How are you?', title_text)
        time.sleep(2)

        message_title_text = self.browser.find_element_by_class_name('judul').text
        self.assertIn("What they're feeling . . .", message_title_text)
        time.sleep(2)

    def test_for_html_css_styling(self):
        self.browser.get('http://terlalu-sibuk.herokuapp.com/lab6/')
        time.sleep(2)

        body_background = self.browser.find_element_by_tag_name('body').value_of_css_property("background")
        self.assertIn("linear-gradient(to right, rgb(212, 41, 0), rgb(255, 133, 63))",body_background )
        time.sleep(2)

        judul_color = self.browser.find_element_by_class_name('judul').value_of_css_property("color")
        self.assertEquals("rgba(153, 153, 153, 1)",judul_color )
        time.sleep(2)
