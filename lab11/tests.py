from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import web_login, homepage, data
import unittest
import time

# Create your tests here.
class Lab11_Test(TestCase):
    def test_lab_11_url_is_exist(self):
        response = Client().get('/lab11/')
        self.assertEqual(response.status_code, 200)

    def test_lab_11_booklist_is_exist(self):
        response = Client().get('/lab11/book-list/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_data_exists(self):
        response = Client().get('/lab11/book-list/data/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_like_exists(self):
        response = Client().get('/lab11/book-list/like/')
        self.assertEqual(response.status_code, 200)
    
    def test_json_data_url_unlike_exists(self):
        response = Client().get('/lab11/book-list/unlike/')
        self.assertEqual(response.status_code, 200)

    def test_json_data_url_getlike_exists(self):
        response = Client().get('/lab11/book-list/get-like/')
        self.assertEqual(response.status_code, 200)
    
    def test_lab11_using_login_template(self):
        response = Client().get('/lab11/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_lab11_using_bookspage_template(self):
        response = Client().get('/lab11/book-list/')
        self.assertTemplateUsed(response, 'bookspage.html')

    def test_lab11_using_weblogin_func(self):
        found = resolve('/lab11/')
        self.assertEqual(found.func, web_login)

    def test_lab11_using_homepage_func(self):
        found = resolve('/lab11/book-list/')
        self.assertEqual(found.func, homepage)

    def test_lab11_using_data_func(self):
        found = resolve('/lab11/book-list/data/')
        self.assertEqual(found.func, data)
