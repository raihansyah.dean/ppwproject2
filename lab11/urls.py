from django.contrib import admin
from django.urls import path, include
from . import views as fungsidiviews
from django.contrib.auth import views
from django.conf import settings


urlpatterns = [
    path('', fungsidiviews.web_login, name='web_login'),
    path('book-list/', fungsidiviews.homepage, name='homepage'),
    path('book-list/data/', fungsidiviews.data, name='books-list'),

    path('login/',  views.LoginView.as_view(), name="login"),
    path('logout/', views.LogoutView.as_view(),  {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),

    path('auth/', include('social_django.urls', namespace='social')),
    path('book-list/like/', fungsidiviews.like),
    path('book-list/unlike/', fungsidiviews.unlike),
    path('book-list/get-like/', fungsidiviews.get_like),
]
